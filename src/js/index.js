"use strict";
let btnready = document.querySelector('.btn-ready');
let model = document.querySelector('.model')
let play = document.querySelector('.play');
let ready = document.querySelector('.ready');
let choice = document.querySelector('.choice');
let eyeVerif = false;
let mouthVerif = false;
let necklaceVerif = false;
let stoptimer = false;
let curentTime = 0;
let round = 0;
let life = 3;
// let count = 0;
let listePhrase = [
  { nom: "Simone la Girafe", img1: "./picture/girafe-color.png", img2: "./picture/girafe-noir-et-blanc.png", img3: "./picture/eye-girafe.png", img4: "./picture/mouth-girafe.png", img5: "./picture/necklace-girafe.png", img6: "./picture/eye-trap-girafe.png" }, //objet
  { nom: "Jack le Gorille", img1: "./picture/Gorille-color.png", img2: "./picture/Gorille-noir-et-blanc.png", img3: "./picture/gorille-bow-tie.jpg", img4: "./picture/Gorille-flower.png", img5: "./picture/gorille-mustache.jpg", img6: "./picture/gorille-bow-tie-green.jpg" },
  { nom: "Gisèle le Panda", img1: "./picture/panda-color.png", img2: "./picture/panda-noir-et-blanc.png", img3: "./picture/panda-tiare.png", img4: "./picture/panda-robe.png", img5: "./picture/Panda-eye.jpg", img6: "./picture/Panda-eye-yellow.jpg" }
];

let btnNomAnimal = document.querySelector('#btn-animal');
let choix = '';
function getRandomInt(min, max) {
  number = Math.floor(Math.random() * (max - min + 1)) + min;
  return number;
}

function verif() {
  if (eyeVerif === true && mouthVerif === true && necklaceVerif === true && round === 2) {
    alert('Bravo !! Tu as gagné !');
    stoptimer = true;
    document.location.reload(true);
  }
  if (eyeVerif === true && mouthVerif === true && necklaceVerif === true && round <= 1) {
    eyeVerif = false;
    mouthVerif = false;
    necklaceVerif = false;
    stoptimer = false;
    round++;

    console.log('verif true');

    let affichageRandom = listePhrase[getRandomInt(0, 2)];

    console.log(affichageRandom.nom);

    let part1 = document.createElement('img');
    part1.src = affichageRandom.img1;
    part1.className = 'girafe';
    model.innerHTML = "";
    model.appendChild(part1);

    let part2 = document.createElement('img');
    part2.src = affichageRandom.img2;
    part2.className = 'girafe-n-b';
    play.innerHTML = "";
    play.appendChild(part2);

    // AFFICHER LES ELEMENTS DANS LA PARTIE TROIS deuxième
    // Part eye girafe, tiara panda, bow-tie gorilla
    let part3a = document.createElement('img');
    part3a.className = 'eye-g';
    part3a.src = affichageRandom.img3;
    choice.innerHTML = "";
    choice.appendChild(part3a);
    part3a.addEventListener('click', function () {
      play.appendChild(part3a);
      eyeVerif = true;
      if (affichageRandom === listePhrase[0]) {
        //eye girafe
        part3a.style.position = 'relative';
        part3a.style.width = '35%';
        part3a.style.marginTop = '-323px';
        part3a.style.marginLeft = '172px';
        part3a.style.float = 'left';

      } else if (affichageRandom === listePhrase[1]) {
        //bow-tie gorilla
        part3a.style.position = 'relative';
        part3a.style.width = '20%';
        part3a.style.marginTop = '-108px';
        part3a.style.marginLeft = '227px';
        part3a.style.float = 'left';

      } else if (affichageRandom === listePhrase[2]) {
        //tiara panda
        part3a.style.position = 'relative';
        part3a.style.width = '33%';
        part3a.style.marginTop = '-329px';
        part3a.style.marginLeft = '169px';
        part3a.style.float = 'left';
      }
      verif();
    });

    //Part mouth girafe, dress panda, flower gorilla
    let part3b = document.createElement('img');
    part3b.className = 'new1';
    part3b.src = affichageRandom.img4;
    choice.appendChild(part3b);
    part3b.addEventListener('click', function () {
      play.appendChild(part3b);
      mouthVerif = true;
      if (affichageRandom === listePhrase[0]) {
        //mouth girafe
        part3b.style.position = 'relative';
        part3b.style.width = '12%';
        part3b.style.marginTop = '-121px';
        part3b.style.marginLeft = '234px';
        part3b.style.float = 'left';

      } else if (affichageRandom === listePhrase[1]) {
        //folwer gorilla
        part3b.style.position = 'relative';
        part3b.style.width = '8%';
        part3b.style.marginTop = '-80px';
        part3b.style.marginLeft = '355px';
        part3b.style.float = 'left';

      } else if (affichageRandom === listePhrase[2]) {

        //dress panda
        part3b.style.position = 'relative';
        part3b.style.width = '38%';
        part3b.style.marginTop = '-130px';
        part3b.style.marginLeft = '165px';
        part3b.style.float = 'left';
      }
      verif();
    });

    //Part necklace girafe, eye panda, mustache gorilla
    let part3c = document.createElement('img');
    part3c.className = 'new2';
    part3c.src = affichageRandom.img5;
    choice.appendChild(part3c);
    part3c.addEventListener('click', function () {
      play.appendChild(part3c);
      necklaceVerif = true;
      if (affichageRandom === listePhrase[0]) {
        //necklace girafe
        part3c.style.position = 'relative';
        part3c.style.width = '20%';
        part3c.style.marginTop = '-118px';
        part3c.style.marginLeft = '234px';
        part3c.style.float = 'left';

      } else if (affichageRandom === listePhrase[1]) {
        //mustache gorilla
        part3c.style.position = 'relative';
        part3c.style.width = '22%';
        part3c.style.marginTop = '-182px';
        part3c.style.marginLeft = '226px';
        part3c.style.float = 'left';

      } else if (affichageRandom === listePhrase[2]) {

        //eye panda
        part3c.style.position = 'relative';
        part3c.style.width = '20%';
        part3c.style.marginTop = '-245px';
        part3c.style.marginLeft = '203px';
        part3c.style.float = 'left';
      }
      verif();
    });
    let eyeTrap2 = document.createElement('img');
    eyeTrap2.src = affichageRandom.img6;
    eyeTrap2.className = 'eyeTrap';
    choice.appendChild(eyeTrap2);
    eyeTrap2.addEventListener('click', function () {
      life--;
      alert(`il te reste ${life} vie`);
      if (life === 0) {
        alert('tu as perdu');
        document.location.reload(true);
      }
    })
  }
  else {
    console.log(round);
  }
}

btnNomAnimal.addEventListener('click', function () {
  let destination = document.querySelector('#target-phrase');
  destination.innerHTML = '';
  destination.appendChild(randomPhrase());
})

let number;

function randomPhrase() {
  let phrase = document.createElement('div');
  phrase.classList.add('phrase'); //on ajoute une class qui s'appelle phrase à la div

  choix = listePhrase[getRandomInt(0, listePhrase.length - 1)];

  phrase.innerHTML = choix.nom;

  // console.log(listePhrase[0]);
  return phrase;
}
btnready.addEventListener('click', function () {
  // //AJOUT DU CHRONO DOM
  function progress(timeLimit) {
    let main = document.querySelector('main');

    let progressbar = document.createElement('div');
    progressbar.setAttribute("class", "progress-bar");
    main.appendChild(progressbar);

    let prg = document.createElement('div');
    prg.setAttribute("class", "progress");
    progressbar.appendChild(prg);

    let id = setInterval(frame, 50);//50

    function frame() {
      if (stoptimer === false) {
        curentTime += 50;
        if (curentTime >= timeLimit) {
          clearInterval(id);
          if (curentTime === timeLimit) {
            alert('Tu as perdu');
            document.location.reload(true);
          }
        }
        prg.style.width = ((curentTime / timeLimit) * 100) + "%";
      }
    }
  }
  progress(15000);//on rentre des millisecondes, c'est là que l'on change le temps 15000

  //AFFICHAGE

  let affichageFirst = listePhrase[getRandomInt(0, 2)];
  affichageFirst = choix;

  //AFFICHER LE MODELE
  let part1 = document.createElement('img');
  part1.src = affichageFirst.img1;
  part1.className = 'girafe';
  model.innerHTML = "";
  model.appendChild(part1);

  //AFFICHER LE PLAY
  let part2 = document.createElement('img');
  part2.src = affichageFirst.img2;
  part2.className = 'girafe-n-b';
  play.innerHTML = "";
  play.appendChild(part2);

  //AFFICHER LES ELEMENTS DES ANIMAUX DE LA PREMIERE ETAPE

  //EYE GIRAFE, BOW-TIE GORILLA, TIARA PANDA

  let eye = document.createElement('img');
  eye.className = 'eye-g';
  eye.src = affichageFirst.img3;
  choice.innerHTML = "";
  choice.appendChild(eye);
  eye.addEventListener('click', function () {
    play.appendChild(eye);
    eyeVerif = true;
    if (affichageFirst === listePhrase[0]) {
      //eye girafe
      eye.style.position = 'relative';
      eye.style.width = '35%';
      eye.style.marginTop = '-323px';
      eye.style.marginLeft = '172px';
      eye.style.float = 'left';

    } else if (affichageFirst === listePhrase[1]) {
      //bow-tie gorilla
      eye.style.position = 'relative';
      eye.style.width = '20%';
      eye.style.marginTop = '-108px';
      eye.style.marginLeft = '227px';
      eye.style.float = 'left';

    } else if (affichageFirst === listePhrase[2]) {
      //tiara panda
      eye.style.position = 'relative';
      eye.style.width = '33%';
      eye.style.marginTop = '-329px';
      eye.style.marginLeft = '169px';
      eye.style.float = 'left';
    }
    verif();
  });

  let mouth = document.createElement('img');
  mouth.className = 'mouth-g';
  mouth.src = affichageFirst.img4;
  choice.appendChild(mouth);
  mouth.addEventListener('click', function () {
    play.appendChild(mouth);
    mouthVerif = true;
    if (affichageFirst === listePhrase[0]) {
      //mouth girafe
      mouth.style.position = 'relative';
      mouth.style.width = '12%';
      mouth.style.marginTop = '-121px';
      mouth.style.marginLeft = '234px';
      mouth.style.float = 'left';

    } else if (affichageFirst === listePhrase[1]) {

      //folwer gorilla
      mouth.style.position = 'relative';
      mouth.style.width = '8%';
      mouth.style.marginTop = '-80px';
      mouth.style.marginLeft = '355px';
      mouth.style.float = 'left';

    } else if (affichageFirst === listePhrase[2]) {
      //dress panda
      mouth.style.position = 'relative';
      mouth.style.width = '38%';
      mouth.style.marginTop = '-130px';
      mouth.style.marginLeft = '165px';
      mouth.style.float = 'left';
    }
    verif();
  });

  let necklace = document.createElement('img');
  necklace.className = 'necklace-g';
  necklace.src = affichageFirst.img5;
  choice.appendChild(necklace);
  necklace.addEventListener('click', function () {
    play.appendChild(necklace);
    necklaceVerif = true;

    if (affichageFirst === listePhrase[0]) {
      //necklace girafe
      necklace.style.position = 'relative';
      necklace.style.width = '20%';
      necklace.style.marginTop = '-118px';
      necklace.style.marginLeft = '234px';
      necklace.style.float = 'left';

    } else if (affichageFirst === listePhrase[1]) {
      //mustache gorilla
      necklace.style.position = 'relative';
      necklace.style.width = '22%';
      necklace.style.marginTop = '-182px';
      necklace.style.marginLeft = '226px';
      necklace.style.float = 'left';

    } else if (affichageFirst === listePhrase[2]) {
      //eye panda
      necklace.style.position = 'relative';
      necklace.style.width = '20%';
      necklace.style.marginTop = '-243px';
      necklace.style.marginLeft = '203px';
      necklace.style.float = 'left';
    }
    verif();
  });

  let eyeTrap = document.createElement('img');
  eyeTrap.src = affichageFirst.img6;
  eyeTrap.className = 'eyeTrap';
  choice.appendChild(eyeTrap);
  eyeTrap.addEventListener('click', function () {
    life--;
    alert(`il te reste ${life} vie`);
    if (life === 0) {
      alert('tu as perdu');
      document.location.reload(true);
    }
  })

});


